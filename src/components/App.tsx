import { ChangeEventHandler, useState } from "react";
import { Link } from "react-router-dom";

const App = () => {
  return (
    <>
      <h1>ユーザー管理システム</h1>
      <Link to="/users">一覧</Link>
    </>
  );
};

export default App;
