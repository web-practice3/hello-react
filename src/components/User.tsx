import axiosClient from "@aspida/axios";
import { Link, useLoaderData } from "react-router-dom";
import api from "../api/$api";
import { User } from "../api/@types";

const loader = async ({ params }: { params: { id?: string } }) => {
  if (!params.id) {
    return {
      user: {
        id: 0,
        first_name: "",
        last_name: "",
        birthdate: "",
        location: "",
      },
    };
  }
  const client = api(axiosClient());
  const res = await client.users._userId(params.id).get();
  return { user: res.body };
};

const UserDetail = () => {
  const { user } = useLoaderData() as { user: User };

  return (
    <>
      <h1>ユーザー</h1>
      <Link to="/users">戻る</Link>
      <dl>
        <dt>ID</dt>
        <dd>{user.id}</dd>
        <dt>First Name</dt>
        <dd>{user.first_name}</dd>
        <dt>Last Name</dt>
        <dd>{user.last_name}</dd>
        <dt>Birth Of Date</dt>
        <dd>{user.birthdate}</dd>
        <dt>Location</dt>
        <dd>{user.location}</dd>
      </dl>
    </>
  );
};

export default UserDetail;
export { loader };
