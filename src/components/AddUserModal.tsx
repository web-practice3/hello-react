import { Button, Form, Modal } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { User } from "../api/@types";
import { useEffect } from "react";

const AddUserModal = ({
  show,
  onClose,
  onSubmit,
}: {
  show: boolean;
  onClose: () => void;
  onSubmit: (values: User) => void;
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<User>();

  useEffect(() => {
    // モーダルが閉じられた時、入力値やバリデーションを初期化する
    if (!show) {
      reset();
    }
  }, [show]);

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header>
        <Modal.Title>ユーザーの追加</Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Form onSubmit={handleSubmit(onSubmit)}>
          {/* First Name */}
          <Form.Group className="mb-3">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              {...register("first_name", {
                required: "First Name is required.",
              })}
              type="text"
              placeholder="First Name"
            />
            {errors.first_name && (
              <Form.Text className="text-danger">
                {errors.first_name.message}
              </Form.Text>
            )}
          </Form.Group>
          {/* Last Name */}
          <Form.Group className="mb-3">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              {...register("last_name", {
                required: "Last Name is required.",
              })}
              type="text"
              placeholder="Last Name"
            />
            {errors.last_name && (
              <Form.Text className="text-danger">
                {errors.last_name.message}
              </Form.Text>
            )}
          </Form.Group>
          {/* Birth Of Date */}
          <Form.Group className="mb-3">
            <Form.Label>Birth Of Date</Form.Label>
            <Form.Control
              {...register("birthdate", {
                required: "Birth Of Date is required.",
              })}
              type="date"
              placeholder="Birth Of Date"
            />
            {errors.birthdate && (
              <Form.Text className="text-danger">
                {errors.birthdate.message}
              </Form.Text>
            )}
          </Form.Group>
          {/* Location */}
          <Form.Group className="mb-3">
            <Form.Label>Location</Form.Label>
            <Form.Select {...register("location")} aria-label="Location">
              <option value="JPN">JPN</option>
              <option value="USA">USA</option>
            </Form.Select>
          </Form.Group>
        </Form>
      </Modal.Body>

      <Modal.Footer>
        <Button variant="secondary" onClick={onClose}>
          閉じる
        </Button>
        <Button variant="primary" onClick={handleSubmit(onSubmit)}>
          追加
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default AddUserModal;
