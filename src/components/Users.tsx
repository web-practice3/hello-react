import { Link } from "react-router-dom";
import { Button, Table } from "react-bootstrap";
import axiosClient from "@aspida/axios";
import useAspidaSWR from "@aspida/swr";
import { useState } from "react";

import AddUserModal from "./AddUserModal";
import api from "../api/$api";
import { User } from "../api/@types";

const client = api(axiosClient());

const Users = () => {
  const [show, setShow] = useState(false);
  const [condition] = useState({
    limit: 20,
    offset: 0,
  });
  const { data, mutate } = useAspidaSWR(client.users, "get", {
    query: condition,
  });

  const addUser = async (user: User) => {
    setShow(false);
    try {
      await client.users.$post({
        body: user,
      });
    } catch (e) {
      console.log(e);
    }
    mutate();
  };

  return (
    <>
      <h1>ユーザー一覧</h1>

      <Button variant="primary" onClick={() => setShow(true)}>
        <i className="bi bi-person-add"></i>ユーザー追加
      </Button>
      <AddUserModal
        show={show}
        onClose={() => setShow(false)}
        onSubmit={addUser}
      />

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birth Of Date</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {data?.body.map((user) => (
            <tr key={user.id}>
              <td>
                <Link to={`/users/${user.id}`}>{user.id}</Link>
              </td>
              <td>{user.first_name}</td>
              <td>{user.last_name}</td>
              <td>{user.birthdate}</td>
              <td>{user.location}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default Users;
