import React from "react";
import ReactDOM from "react-dom/client";
import { Navigate, createHashRouter, RouterProvider } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";

import App from "./components/App";
import Users from "./components/Users";
import User, { loader as userLoader } from "./components/User";

const router = createHashRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/users",
    element: <Users />,
  },
  {
    path: "/users/:id",
    element: <User />,
    loader: userLoader,
  },
  {
    path: "*",
    element: <Navigate replace to="/" />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as Element).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
