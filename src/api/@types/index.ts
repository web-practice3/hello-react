/* eslint-disable */
export type User = {
  /** User ID */
  id: number
  /** User first name */
  first_name: string
  /** User last name */
  last_name: string
  /** User birthdate */
  birthdate: string
  /** User location */
  location: string
}
