/* eslint-disable */
import type * as Types from '../@types'

export type Methods = {
  /** Get all users */
  get: {
    query?: {
      /** Limit of users */
      limit?: number | undefined
      /** Offset of users */
      offset?: number | undefined
    } | undefined

    status: 200
    /** OK */
    resBody: Types.User[]
  }

  /** Create new user */
  post: {
    status: 200
    /** OK */
    resBody: Types.User
    reqBody: Types.User
  }
}
