/* eslint-disable */
import type * as Types from '../../@types'

export type Methods = {
  /** Get user by id */
  get: {
    status: 200
    /** OK */
    resBody: Types.User
  }

  /** Update user by id */
  patch: {
    status: 200
    /** OK */
    resBody: Types.User
  }

  /** Delete user by id */
  delete: {
    status: 200
  }
}
