import type { AspidaClient, BasicHeaders } from 'aspida';
import { dataToURLString } from 'aspida';
import type { Methods as Methods_1xhiioa } from './users';
import type { Methods as Methods_kyp39q } from './users/_userId';

const api = <T>({ baseURL, fetch }: AspidaClient<T>) => {
  const prefix = (baseURL === undefined ? '/api' : baseURL).replace(/\/$/, '');
  const PATH0 = '/users';
  const GET = 'GET';
  const POST = 'POST';
  const DELETE = 'DELETE';
  const PATCH = 'PATCH';

  return {
    users: {
      _userId: (val1: number | string) => {
        const prefix1 = `${PATH0}/${val1}`;

        return {
          /**
           * Get user by id
           * @returns OK
           */
          get: (option?: { config?: T | undefined } | undefined) =>
            fetch<Methods_kyp39q['get']['resBody'], BasicHeaders, Methods_kyp39q['get']['status']>(prefix, prefix1, GET, option).json(),
          /**
           * Get user by id
           * @returns OK
           */
          $get: (option?: { config?: T | undefined } | undefined) =>
            fetch<Methods_kyp39q['get']['resBody'], BasicHeaders, Methods_kyp39q['get']['status']>(prefix, prefix1, GET, option).json().then(r => r.body),
          /**
           * Update user by id
           * @returns OK
           */
          patch: (option?: { config?: T | undefined } | undefined) =>
            fetch<Methods_kyp39q['patch']['resBody'], BasicHeaders, Methods_kyp39q['patch']['status']>(prefix, prefix1, PATCH, option).json(),
          /**
           * Update user by id
           * @returns OK
           */
          $patch: (option?: { config?: T | undefined } | undefined) =>
            fetch<Methods_kyp39q['patch']['resBody'], BasicHeaders, Methods_kyp39q['patch']['status']>(prefix, prefix1, PATCH, option).json().then(r => r.body),
          /**
           * Delete user by id
           */
          delete: (option?: { config?: T | undefined } | undefined) =>
            fetch<void, BasicHeaders, Methods_kyp39q['delete']['status']>(prefix, prefix1, DELETE, option).send(),
          /**
           * Delete user by id
           */
          $delete: (option?: { config?: T | undefined } | undefined) =>
            fetch<void, BasicHeaders, Methods_kyp39q['delete']['status']>(prefix, prefix1, DELETE, option).send().then(r => r.body),
          $path: () => `${prefix}${prefix1}`,
        };
      },
      /**
       * Get all users
       * @returns OK
       */
      get: (option?: { query?: Methods_1xhiioa['get']['query'] | undefined, config?: T | undefined } | undefined) =>
        fetch<Methods_1xhiioa['get']['resBody'], BasicHeaders, Methods_1xhiioa['get']['status']>(prefix, PATH0, GET, option).json(),
      /**
       * Get all users
       * @returns OK
       */
      $get: (option?: { query?: Methods_1xhiioa['get']['query'] | undefined, config?: T | undefined } | undefined) =>
        fetch<Methods_1xhiioa['get']['resBody'], BasicHeaders, Methods_1xhiioa['get']['status']>(prefix, PATH0, GET, option).json().then(r => r.body),
      /**
       * Create new user
       * @returns OK
       */
      post: (option: { body: Methods_1xhiioa['post']['reqBody'], config?: T | undefined }) =>
        fetch<Methods_1xhiioa['post']['resBody'], BasicHeaders, Methods_1xhiioa['post']['status']>(prefix, PATH0, POST, option).json(),
      /**
       * Create new user
       * @returns OK
       */
      $post: (option: { body: Methods_1xhiioa['post']['reqBody'], config?: T | undefined }) =>
        fetch<Methods_1xhiioa['post']['resBody'], BasicHeaders, Methods_1xhiioa['post']['status']>(prefix, PATH0, POST, option).json().then(r => r.body),
      $path: (option?: { method?: 'get' | undefined; query: Methods_1xhiioa['get']['query'] } | undefined) =>
        `${prefix}${PATH0}${option && option.query ? `?${dataToURLString(option.query)}` : ''}`,
    },
  };
};

export type ApiInstance = ReturnType<typeof api>;
export default api;
